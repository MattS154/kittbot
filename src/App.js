import handlers from './config/handlers.js';

export default async function () {
    const handler = process.env.CMD_HANDLER;

    if (!handler) throw new Error('Handler configuration missing.');

    const hasHandler = Object.keys(handlers).indexOf(handler) > -1;

    if (!hasHandler) throw new Error(`Handler ${handler} is not configured.`);

    await handlers[handler]();
}

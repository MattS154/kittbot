import * as dotenv from 'dotenv';
import App from './App.js';

dotenv.config();

App();

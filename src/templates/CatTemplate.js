import Template from "../models/Template.js";

export default class CatTemplate extends Template {
    compile(data) {
        super.compile();
        return `${data.url.href}`
    }
}

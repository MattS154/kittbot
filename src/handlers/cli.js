import runner from "./runner.js";

export default async function cli() {
    var result = await runner.execute(process.argv.slice(2));

    console.log(result.data);
}

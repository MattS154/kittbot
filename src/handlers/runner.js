import yargsParser from "yargs-parser";
import commands from "../config/commands.js";
import ErrorResponse from "../models/ErrorResponse.js";

const runner = {};

runner.execute = async function(input) {
    console.log(input);
    const args = yargsParser(input);

    const commandName = args._[0];

    const command = commands[commandName];

    if (!command) {
        return new ErrorResponse(`command '${commandName}' not found.`);
    }

    return await (new command()).run(args._.slice(1));
}

export default runner;

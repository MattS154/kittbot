import Discord from "discord.js";
import clearFormatting from "../helpers/clearFormatting.js";
import runner from "./runner.js";

export default function discord() {

    const client = new Discord.Client();

    //Listen to the event that signals the bot is ready to start working
    client.on("ready", () => {
      console.log(`logged in as ${client.user.tag}`);
    });

    //Listen to new messages on the server
    client.on("message", async (message) => {
        //"Ignore the message if the bot authored it"
        if (message.author.bot) return;

        //If the doesn't specifically mention the bot, return
        if (message.content.includes("@here") || message.content.includes("@everyone")) return;

        //Return if the message doesn't mention the bot
        if (!message.mentions.has(client.user.id)) return;

        console.log(message.content);

        let content = message.content;

        // Remove leading mention
        content = message.content.replace(/<@!\d+>/, "").trim();

        content = clearFormatting(content);

        var result = await runner.execute(content);

        message.reply(result.getMessage());
    });

    //Login to the server using bot token
    client.login(process.env.DISCORD_BOT_TOKEN);
}

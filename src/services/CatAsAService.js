import fetch from "node-fetch";
import cataas from "../config/cataas.js";
import CataasResponse from "../models/CataasResponse.js";

let service = {};

service.cat = async function (tag) {
    let url = new URL(cataas.origin)

    url.pathname = '/cat'

    if (Boolean(tag)) {
        url.pathname += `/${tag}`;
    }

    url.searchParams.set('json', true)

    let response = await fetch(url.href);

    if (response.status !== 200) {
        throw new Error(`${response.status}: ${response.statusText}`);
    }

    return new CataasResponse(await response.json());
}

export default service;

import cli from '../handlers/cli.js';
import discord from '../handlers/discord.js';

export default {
    cli,
    discord,
};

const markdownTags = [
    '*',
    '_',
    '`',
    '~',
    '>',
]

export default function clearFormatting(string) {
    markdownTags.forEach(function (tag) {
        string = string.replaceAll(tag, '');
    });

    return string;
}

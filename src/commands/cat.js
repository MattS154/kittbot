import CatResponse from "../models/CatResponse.js";
import Command from "../models/Command.js";
import CatAsAService from "../services/CatAsAService.js";

export default class Cat extends Command {
    command = 'cat [tag]';
    description = 'Cat.';
    options = {
        tag: {
            type: 'string',
            default: null
        }
    };

    async handler(options) {
        let response = await CatAsAService.cat(options.pop());
        return new CatResponse(response);
    }
}

import CatTemplate from "../templates/CatTemplate.js";
import CommandResponse from "./CommandResponse.js";

export default class CatResponse extends CommandResponse {
    constructor(data) {
        super(data);
        this.data = data;
        this.template = new CatTemplate();
    }
}

import ErrorResponse from "./ErrorResponse.js";

export default class Command {
    options;

    async run(args) {
        try {
            return await this.handler(args);
        } catch (e) {
            return new ErrorResponse(e.message);
        }
    }
}

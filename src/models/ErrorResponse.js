import ErrorTemplate from "../templates/ErrorTemplate.js";
import CommandResponse from "./CommandResponse.js";

export default class ErrorResponse extends CommandResponse {
    constructor(data) {
        super(data);
        this.data = data;
        this.template = new ErrorTemplate();
    }
}

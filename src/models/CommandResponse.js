export default class CommandResponse {
    template;
    data;

    getMessage() {
        return this.template.compile(this.data);
    }
}

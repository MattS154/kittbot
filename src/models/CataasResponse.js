import moment from "moment";
import cataas from "../config/cataas.js";

export default class CataasResponse {

    constructor({id, created_at, tags, url}) {
        this.id = id;
        this.created_at = moment(created_at);
        this.tags = tags;
        this.url = new URL(`${cataas.origin}${url}`);
    }

}
